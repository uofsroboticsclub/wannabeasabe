import sys
import os
import struct
from serial import Serial
from enum import Enum
from time import sleep
import numpy as np

hardware_serial_port = "/dev/serial/by-id/usb-Arduino_LLC_Arduino_Leonardo-if00"
virtual_serial_port = "/tmp/ttyV0"

MESSAGE_BODY_COUNT = 2

if os.path.exists(virtual_serial_port):
    serial_port = virtual_serial_port
else:
    serial_port = hardware_serial_port

class MessageID(Enum):
    RESPONSE = 0
    READ_ANALOG = 1
    READ_DIGITAL = 2
    ECHO_TEST = 3
    READ_DIST = 4
    SET_MOTORS = 5

class MicrocontrollerInterface:
    def __init__(self):
        self.serial = Serial(serial_port, timeout=1)

    def __del__(self):
        self.serial.close()

    def _send_message(self, messageID, arg0: int=0, arg1: int=0, send_signed: bool = False):
        format_char = "l" if send_signed else "L"
        format = "=B" + format_char * MESSAGE_BODY_COUNT
        dataout = struct.pack(format, messageID.value, arg0, arg1)
        self.serial.write(dataout)
        # TODO - This line is disabling the timeout, should figure out workaround
        while self.serial.read(1) != b'\x00': pass # flush any debug info out of the buffer
        rawdata = self.serial.read(struct.calcsize(format))
        if len(rawdata) == 0:
            raise RuntimeError("Serial connection to arduino timed out")
        data = struct.unpack(format, rawdata)
        assert data[0] == 0  # is reply to message
        return data[1:]

    def analog_read(self, pinNumber):
        return self._send_message(MessageID.READ_ANALOG, pinNumber)[0]

    def digital_read(self, pinNumber):
        return self._send_message(MessageID.READ_DIGITAL, pinNumber)[0]

    def echo_test(self):
        distance = self._send_message(MessageID.ECHO_TEST)[0]
        if distance < 30000:
            return distance
        else:
            distance = self._send_message(MessageID.ECHO_TEST)[0]
            if distance < 30000:
                return distance
            else:
                return None

    def set_motor_output(self, left_motor, right_motor):
        left_motor = int(np.clip(left_motor, -1, 1)*128)
        right_motor = int(np.clip(right_motor, -1, 1)*128)
        self._send_message(MessageID.SET_MOTORS, left_motor, right_motor, send_signed=True)



if __name__ == "__main__":
    micro = MicrocontrollerInterface()
    # print(micro.analog_read(3))
    # print(micro.digital_read(3))
    while True:
        sleep(0.5)
        print(micro.set_motor_output(-1,1))