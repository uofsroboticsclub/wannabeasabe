// Main file to build for microcontroller

// Pin Connections
const int PIN_ULTRASONIC_TRIG = 3;
const int PIN_ULTRASONIC_ECHO = 2;

enum class MessageID: uint8_t {
	RESPONSE = 0,
    READ_ANALOG = 1,
    READ_DIGITAL = 2,
    ECHO_TEST = 3,
    READ_DIST = 4,
    SET_MOTORS = 5,
};

struct Message{
  MessageID ID;
  uint32_t data0;
  uint32_t data1;
};

unsigned long echotest()
{
  digitalWrite(PIN_ULTRASONIC_TRIG, HIGH);
  delayMicroseconds(10);
  digitalWrite(PIN_ULTRASONIC_TRIG, LOW);
  while(digitalRead(PIN_ULTRASONIC_ECHO) == LOW);
  unsigned long startTime = micros();
  while(digitalRead(PIN_ULTRASONIC_ECHO) == HIGH);
  return (micros() - startTime) / 5.8;  // Convert to mm
}

void set_motors(Message message) {
  int32_t left_motor = message.data0;
  int32_t right_motor = message.data1;
  Serial.println (left_motor);
  Serial.println (right_motor);
}


void setup() {
	Serial.begin(9600);
	while (!Serial);
	ultrasonic_init();
	Serial.println("Done initializing.");
}

Message processNewData(Message message){
	switch (message.ID){
		case MessageID::READ_ANALOG:
			message.data0 = analogRead(message.data0);
			break;
		case MessageID::READ_DIGITAL:
			message.data0 = digitalRead(message.data0);
			break;
		case MessageID::ECHO_TEST:
			message.data0 = echotest();
			break;
		case MessageID::READ_DIST:
			message = ultrasonic_getDistance();
			break;
      case MessageID::SET_MOTORS:
      set_motors (message);
	}
	return message;
}

void loop() {
  	if (Serial.available() >= sizeof(Message)) { // New data here
		Message message;
		char* messagePointer = (char*) &message;
     	for (int i = 0; i < sizeof(Message); i++) *messagePointer++ = Serial.read(); 
  		Message response = processNewData(message);
		response.ID = MessageID::RESPONSE;
		Serial.write((byte)0x00);
		Serial.write((const uint8_t *)&response, sizeof(Message));
  	}
}
