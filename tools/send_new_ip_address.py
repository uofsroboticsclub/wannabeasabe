#!/home/pi/.local/bin/python3

# This file is executed by rc.local at bootup, and sends the current IP address via a MQTT message

import os
import sys
from time import sleep
import netifaces
import requests


root_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../"))
if root_path not in sys.path: sys.path.append(root_path)
print(root_path)
print("sys.path=", sys.path)
from tools.send_text import send_text


if len(sys.argv) > 1:
    print("Waiting for network to initialize...")
    sleep(10)  # This is nessasary when launching at bootup

try:
    ip = netifaces.ifaddresses('wlan0')[netifaces.AF_INET][0]['addr']
    print("Found IP for adaptor wlan0", ip)
except KeyError:
    ip = netifaces.ifaddresses('eth0')[netifaces.AF_INET][0]['addr']
    print("Found IP for adaptor eth0", ip)

print("Sending IP Update request message")
send_text(ip)
base_url = "http://{authstring}@dynupdate.no-ip.com/nic/update?hostname=roboticsrobot.ddns.net&myip={ip}"
r = requests.get(base_url.format(authstring="robotics@ieee.usask.ca:robotpassword", ip=ip))

print(r.status_code, "-", r.text, end="")

if "good" not in r.text and "nochg" not in r.text:
    print("Error setting IP address")
else:
    print("Successfully set IP address")
