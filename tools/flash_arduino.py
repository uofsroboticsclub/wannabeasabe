#!/home/pi/.local/bin/python3

# This script flashes the hex file to the arduino attached to the system.

import sys
import os
import subprocess

root_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../"))
if root_path not in sys.path: sys.path.append(root_path)

from interface.microcontroller_interface import hardware_serial_port

hexfile = root_path + "/microcontroller_core/microcontroller_core.ino.leonardo.hex"
programmerfile = root_path + "/tools/leonardo_uploader"

# This requires avrdude to be installed, and a program from this git repo
# https://github.com/vanbwodonk/leonardoUploader.git

command = [programmerfile, hardware_serial_port, hexfile]
subprocess.run(command)