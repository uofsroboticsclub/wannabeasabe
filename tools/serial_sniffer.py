import sys
import os
import subprocess
from time import sleep
from enum import Enum
import struct
from itertools import islice

root_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../"))
if root_path not in sys.path: sys.path.append(root_path)

from interface.microcontroller_interface import hardware_serial_port, virtual_serial_port, MessageID, MESSAGE_BODY_COUNT

# Requires interceptty from here - https://github.com/geoffmeyers/interceptty


command = ["interceptty", hardware_serial_port, virtual_serial_port]


class Dir(Enum):
    SEND = 0
    RECV = 1

class Printer:
    remaining_data = 0
    message_type = None
    message_ID = None
    message_data = []

    def add_char(self, char: int, dir: Dir):
        # print("Raw data =", char, dir, self.message_type, self.remaining_data)
        if self.remaining_data == 0 and dir == dir.SEND:
            self.remaining_data = 1 + MESSAGE_BODY_COUNT * 4
            self.message_type = Dir.SEND
        if self.remaining_data == 0 and dir == dir.RECV and char == 0:
            self.remaining_data = 2 + MESSAGE_BODY_COUNT * 4
            self.message_type = Dir.RECV
        if self.remaining_data == 2 + MESSAGE_BODY_COUNT * 4:
            self.remaining_data -= 1
        elif self.remaining_data == 1 + MESSAGE_BODY_COUNT * 4:
            self.remaining_data -= 1
            self.message_ID = char
        elif MESSAGE_BODY_COUNT * 4 >= self.remaining_data > 0:
            self.remaining_data -= 1
            self.message_data.append(char)
        if self.message_type is None:
            print(bytes([char]).decode('ascii'), end="")
        if self.remaining_data == 0 and self.message_type is not None:
            self.print_data_transfer()
            self.reset()


    def print_data_transfer(self):
        message_raw = []
        for i in range(MESSAGE_BODY_COUNT):
            message_raw.append(self.message_data[i * 4 : (i + 1) * 4])
        message = [int.from_bytes(bytes(list(x)), byteorder='little') for x in message_raw]
        print("-- Data Transfer - Direction={}, ID={}, Data={}".format(
            self.message_type.name, MessageID(self.message_ID).name, message))

    def reset(self):
        self.remaining_data = 0
        self.message_type = None
        self.message_data = []


printer = Printer()
while True:
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    while process.poll() is None:
        for output_line_raw in process.stdout:
            output_line = output_line_raw.decode('utf-8').strip()
            # print("{" + output_line + "}")
            if output_line == "Backend device was closed.":
                print("-- Interrupted by programmer")
                printer.reset()
            else:
                # Parse the wierd output format into something nicer
                dir = Dir.SEND if output_line[0] == "<" else Dir.RECV
                char_raw = output_line[4:6] if dir == Dir.SEND else output_line[5:7]
                printer.add_char(int(char_raw, 16), dir)
    # We were kicked off the port by the programmer
    # Wait for the reset process to finish, then start polling the port to see when it is ready to start reading again
    state = 0
    lastx = False
    laststate = 0
    while True:
        x = os.path.exists(hardware_serial_port)
        if x != lastx: state += 1
        lastx = x
        if state == 3: break  # When the port has appeared and disapeared the right number of times, we know it's done
        sleep(0.1)
    print("-- Done programming - Ready")